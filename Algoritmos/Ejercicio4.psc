Algoritmo ejercicio4
	Tolerancia <- 0.5*10^(-1)
	ite <- 1
	Escribir 'Ingrese su opcion'
	Escribir '1) Tartaglia'
	Escribir '2) Muller'
	Escribir 'otro salir'
	Leer eleccion
	Si (eleccion==1) Entonces
		// Tartaglia
		a <- 0.6
		b <- 0.2
		c <- -0.4
		p <- b-(a^2)/3
		q <- c-(a*b/3)+2*(a^3)/27
		Si (((4*(p^3)+27*(q^2))/108)<0) Entonces
			Escribir 'D < 0'
			cosDe <- (27^(1/2)*q)/(2*p*(-p)^(1/2))
			ang <- ACOS(cosDe)
			K <- -(a/3)
			X1 <- 2*(-p/3)^(1/2)*COS(ang/3)+K
			X2 <- 2*(-p/3)^(1/2)*COS(ang/3+(2/3*3.141592))+K
			X3 <- 2*(-p/3)^(1/2)*COS(ang/3+(4/3*3.141592))+K
			Escribir 'X1= ',X1
			Escribir 'X2= ',X2
			Escribir 'X3= ',X3
		SiNo
			D <- raiz((4*(p^3)+27*(q^2))/108)
			Escribir D
			Si (D>0) Entonces
				Escribir 'D > 0'
				A1 <- -(q/2)+(((q^2)/4)+((p^3)/27))^(1/2)
				B1 <- -(q/2)-(((q^2)/4)+((p^3)/27))^(1/2)
				K <- -(a/3)
				// Da complejos si la raiz es menor a cero aunque sea raiz impar
				Si (B1<0) Entonces
					X1 <- A1^(1/3)-(-B1)^(1/3)+K
					Escribir 'X1= ',X1
					Escribir 'X2= ',(-(A1^(1/3)-(-B1)^(1/3))/2)+K,' ',((3)^(1/2)*((A1^(1/3)-(-B1)^(1/3))/2)),'i'
					Escribir 'X2= ',(-(A1^(1/3)-(-B1)^(1/3))/2)+K,' ',(-((3)^(1/2))*((A1^(1/3)-(-B1)^(1/3))/2)),'i'
				SiNo
					Si (A1<0) Entonces
						X1 <- B1^(1/3)-(-A1)^(1/3)+K
						Escribir 'X1= ',X1
						Escribir 'X2= ',(-(B1^(1/3)-(-A1)^(1/3))/2)+K,' ',((3)^(1/2)*((B1^(1/3)-(-A1)^(1/3))/2)),'i'
						Escribir 'X2= ',(-(B1^(1/3)-(-A1)^(1/3))/2)+K,' ',(-((3)^(1/2))*((B1^(1/3)-(-A1)^(1/3))/2)),'i'
					SiNo
						X1 <- A1^(1/3)+B1^(1/3)+K
						Escribir 'X1= ',X1
						Escribir 'X2= ',(-(A1^(1/3)+B1^(1/3))/2)+K,' ',((3)^(1/2)*((A1^(1/3)+B1^(1/3))/2)),'i'
						Escribir 'X2= ',(-(A1^(1/3)+B1^(1/3))/2)+K,' ',(-((3)^(1/2))*((A1^(1/3)+B1^(1/3))/2)),'i'
					FinSi
				FinSi
			SiNo
				Escribir 'D = 0'
				K <- -(a/3)
				X1 <- 2*(-q/2)^(1/3)+K
				X2 <- (q/2)^(1/3)+K
				Escribir 'X1= ',X1
				Escribir 'X2= ',X2
				Escribir 'X3= ',X2
			FinSi
		FinSi
	SiNo
		Si (eleccion==2) Entonces
			// Muller
			Escribir 'ingrese X0'
			Leer X0
			Escribir 'ingrese X1'
			Leer X1
			Escribir 'ingrese X2'
			Leer X2
			fx0 <- 5*X0^3+3*X0^2+X0-2
			fx1 <- 5*X1^3+3*X1^2+X1-2
			fx2 <- 5*X2^3+3*X2^2+X2-2
			h0 <- X1-X0
			h1 <- X2-X1
			S0 <- (fx1-fx0)/h0
			S1 <- (fx2-fx1)/h1
			a <- (S1-S0)/(h1-h0)
			b <- a*h1+S1
			c <- fx2
			D <- (b^2-4*a*c)^(1/2)
			Si (abs(b+D)>abs(b-D)) Entonces
				Xr <- X2+((-2*c)/(b+D))
			SiNo
				Xr <- X2+((-2*c)/(b-D))
			FinSi
			Ea <- abs((Xr-X2)/Xr)*100
			Mientras Ea>Tolerancia Hacer
				ite <- ite+1
				X0 <- X1
				X1 <- X2
				X2 <- Xr
				fx0 <- 5*X0^3+3*X0^2+X0-2
				fx1 <- 5*X1^3+3*X1^2+X1-2
				fx2 <- 5*X2^3+3*X2^2+X2-2
				h0 <- X1-X0
				h1 <- X2-X1
				S0 <- (fx1-fx0)/h0
				S1 <- (fx2-fx1)/h1
				a <- (S1-S0)/(h1-h0)
				b <- a*h1+S1
				c <- fx2
				D <- (b^2-4*a*c)^(1/2)
				Si (abs(b+D)>abs(b-D)) Entonces
					Xr <- X2+((-2*c)/(b+D))
				SiNo
					Xr <- X2+((-2*c)/(b-D))
				FinSi
				Ea <- abs((Xr-X2)/Xr)*100
			FinMientras
			Escribir 'iteracion ',ite
			Escribir 'La raiz es ',Xr
			Escribir 'El error aprox. es ',Ea
		FinSi
	FinSi
FinAlgoritmo
