Algoritmo ejercicio1
	Tolerancia <- 0.5*10^(-1)
	ite <- 1
	Escribir 'Ingrese su opcion'
	Escribir '1) Ferrari'
	Escribir '2) Bairstow'
	Escribir 'otro salir'
	Leer eleccion
	Si (eleccion==1) Entonces
		// Ferrari
		A <- 2
		B <- -7
		C <- 0
		D <- 3
		P <- (8*B-3*A^2)/8
		Q <- (8*C-4*A*B+A^3)/8
		R <- (256*D-64*A*C+16*(A^2)*B-3*A^4)/256
		A1 <- -P/2
		B1 <- -R
		C1 <- (4*P*R-(Q^2))/8
		P1 <- B1-(A1^2)/3
		Q1 <- C1-(A1*B1/3)+2*(A1^3)/27
		Si (((4*(P1^3)+27*(Q1^2))/108)<0) Entonces
			Escribir 'Det < 0'
			cosDe <- (27^(1/2)*Q1)/(2*P1*(-P1)^(1/2))
			ang <- ACOS(cosDe)
			K <- -(A1/3)
			U <- 2*(-P1/3)^(1/2)*COS(ang/3)+K
			V <- (2*U-P)^(1/2)
			W <- Q/(-2*V)
			X1 <- (V+(V^2-4*(U-W))^(1/2))/2-A/4
			X2 <- (V-(V^2-4*(U-W))^(1/2))/2-A/4
			X3 <- (-V+(V^2-4*(U+W))^(1/2))/2-A/4
			X4 <- (-V-(V^2-4*(U+W))^(1/2))/2-A/4
			Escribir 'X1= ',X1
			Escribir 'X2= ',X2
			Escribir 'X3= ',X3
			Escribir 'X4= ',X4
		SiNo
			Det <- ((4*(P1^3)+27*(Q1^2))/108)^(1/2)
			Escribir Det
			Si (Det>0) Entonces
				Escribir 'Det > 0'
				A2 <- -Q1/2+Det
				B2 <- -Q1/2+Det
				K <- -(A1/3)
				U <- A2^(1/3)+B2^(1/3)+K
				V <- (2*U-P)^(1/2)
				W <- Q/(-2*V)
				X1 <- (V+(V^2-4*(U-W))^(1/2))/2-A/4
				X2 <- (V-(V^2-4*(U-W))^(1/2))/2-A/4
				X3 <- (-V+(V^2-4*(U+W))^(1/2))/2-A/4
				X4 <- (-V-(V^2-4*(U+W))^(1/2))/2-A/4
				Escribir 'X1= ',X1
				Escribir 'X2= ',X2
				Escribir 'X3= ',X3
				Escribir 'X4= ',X4
			SiNo
				Escribir 'Det = 0'
				K <- -(A1/3)
				U <- 2*(-Q1/2)^(1/3)+K
				V <- (2*U-P)^(1/2)
				W <- Q/(-2*V)
				X1 <- (V+(V^2-4*(U-W))^(1/2))/2-A/4
				X2 <- (V-(V^2-4*(U-W))^(1/2))/2-A/4
				X3 <- (-V+(V^2-4*(U+W))^(1/2))/2-A/4
				X4 <- (-V-(V^2-4*(U+W))^(1/2))/2-A/4
				Escribir 'X1= ',X1
				Escribir 'X2= ',X2
				Escribir 'X3= ',X3
				Escribir 'X4= ',X4
				Escribir 'X1= ',X1
				Escribir 'X2= ',X2
				Escribir 'X3= ',X2
			FinSi
		FinSi
	SiNo
		Si (eleccion==2) Entonces
			// Bairstow
			Escribir 'Ingrese R0'
			Leer R0
			Escribir 'Ingreses S0'
			Leer S0
			A4 <- 1
			A3 <- 2
			A2 <- -7
			A1 <- 0
			A0 <- 3
			B4 <- A4
			B3 <- A3+(R0*B4)
			B2 <- A2+(R0*B3)+(S0*B4)
			B1 <- A1+(R0*B2)+(S0*B3)
			B0 <- A0+(R0*B1)+(S0*B2)
			C4 <- B4
			C3 <- B3+(R0*C4)
			C2 <- B2+(R0*C3)+(S0*C4)
			C1 <- B1+(R0*C2)+(S0*C3)
			deltaR <- (((-1)*B1*C2)+(B0*C3))/((C2*C2)-(C1*C3))
			deltaS <- (((-1)*C2*B0)+(C1*B1))/((C2*C2)-(C1*C3))
			R <- R0+deltaR
			S <- S0+deltaS
			Ear <- abs(deltaR/R)*100
			Eas <- abs(deltaS/S)*100
			Mientras (Ear>Tolerancia O Eas>Tolerancia) Hacer
				ite <- ite+1
				R0 <- R
				S0 <- S
				B4 <- A4
				B3 <- A3+(R0*B4)
				B2 <- A2+(R0*B3)+(S0*B4)
				B1 <- A1+(R0*B2)+(S0*B3)
				B0 <- A0+(R0*B1)+(S0*B2)
				C4 <- B4
				C3 <- B3+(R0*C4)
				C2 <- B2+(R0*C3)+(S0*C4)
				C1 <- B1+(R0*C2)+(S0*C3)
				deltaR <- (((-1)*B1*C2)+(B0*C3))/((C2*C2)-(C1*C3))
				deltaS <- (((-1)*C2*B0)+(C1*B1))/((C2*C2)-(C1*C3))
				R <- R0+deltaR
				S <- S0+deltaS
				Ear <- abs(deltaR/R)*100
				Eas <- abs(deltaS/S)*100
			FinMientras
			X1 <- (R+(R^2+4*S)^(1/2))/2
			X2 <- (R-(R^2+4*S)^(1/2))/2
			// division sintetica
			R <- -3
			S <- 3
			X3 <- (R+(R^2+4*S)^(1/2))/2
			X4 <- (R-(R^2+4*S)^(1/2))/2
			Escribir 'iteracion ',ite
			Escribir 'X1= ',X1
			Escribir 'X2= ',X2
			Escribir 'X3= ',X3
			Escribir 'X4= ',X4
			Escribir 'Ear ',Ear
			Escribir 'Eas ',Eas
		FinSi
	FinSi
FinAlgoritmo
