Algoritmo ejercicio5
	x <- 1
	Dimension datos[3,5]
	Para i<-0 Hasta 4 Hacer
		factorial <- 1
		Para n<-1 Hasta (2*i+1) Hacer
			factorial <- factorial*n
		FinPara
		valorVerdad <- (((-1)^i)*(((x^i)/(3*(i+1)))-(((2^(i+1))*(x^(2*i)))/factorial)))
		total1 <- total1+valorVerdad
		// truncar
		valorTrunc <- Trunc(valorVerdad)
		decimales <- Trunc(abs(valorVerdad-valorTrunc)*100000)/100000
		Si (valorVerdad<0) Entonces
			valorTrunc <- valorTrunc-decimales
		SiNo
			valorTrunc <- valorTrunc+decimales
		FinSi
		total2 <- total2+valorTrunc
		// redondear
		valorRedon <- Trunc(valorVerdad)
		decimales2 <- Trunc(abs(valorVerdad-valorRedon)*10000)
		Si (abs(decimales2*10-decimales*100000)>4) Entonces
			decimales2 <- (decimales2+1)/10000
		SiNo
			decimales2 <- decimales2/10000
		FinSi
		Si (valorVerdad<0) Entonces
			valorRedon <- valorRedon-decimales2
		SiNo
			valorRedon <- valorRedon+decimales2
		FinSi
		total3 <- total3+valorRedon
	FinPara
	Escribir 'valor verdadero= ',total1
	Escribir 'valor truncado= ',total2
	Escribir 'valor redondeado= ',total3
	Er1 <- abs(total1-total2)
	Err1 <- Er1/abs(total1)
	Ep1 <- Err1*100
	Er2 <- abs(total1-total3)
	Err2 <- Er2/abs(total1)
	Ep2 <- Err2*100
	Escribir 'Truncado:'
	Escribir 'Er= ',Er1
	Escribir 'Err= ',Err1
	Escribir 'Ep= ',Ep1
	Escribir 'Redondeado:'
	Escribir 'Er= ',Er2
	Escribir 'Err= ',Err2
	Escribir 'Ep= ',Ep2
FinAlgoritmo
