Algoritmo ejercicio2
	Tolerancia <- 0.5*10^(-1)
	ite <- 1
	Escribir 'Ingrese su opcion'
	Escribir '1) Biseccion'
	Escribir '2) Punto fijo'
	Escribir 'otro salir'
	Leer eleccion
	Si (eleccion==1) Entonces
		// Biseccion
		Escribir 'ingrese X1'
		Leer X1
		Escribir 'ingrese X2'
		Leer X2
		Xr <- (X1+X2)/2
		fx1 <- 0.5-X1+0.2*SEN(X1)
		fx2 <- 0.5-X2+0.2*SEN(X2)
		fxr <- 0.5-Xr+0.2*SEN(Xr)
		Si (fx1*fxr<0) Entonces
			X2 <- Xr
		SiNo
			Si (fx1*fxr>0) Entonces
				X1 <- Xr
			SiNo
				Escribir 'La raiz es ',Xr
			FinSi
		FinSi
		Ea <- 1000
		Mientras Ea>Tolerancia Hacer
			ite <- ite+1
			Xant <- Xr
			Xr <- (X1+X2)/2
			fx1 <- 0.5-X1+0.2*SEN(X1)
			fx2 <- 0.5-X2+0.2*SEN(X2)
			fxr <- 0.5-Xr+0.2*SEN(Xr)
			Si (fx1*fxr<0) Entonces
				X2 <- Xr
			SiNo
				Si (fx1*fxr>0) Entonces
					X1 <- Xr
				SiNo
					Escribir 'La raiz es ',Xr
				FinSi
			FinSi
			Ea <- abs((Xr-Xant)/Xr)*100
		FinMientras
		Escribir 'iteracion ',ite
		Escribir 'La raiz es ',Xr
		Escribir 'El error aprox. es ',Ea
	SiNo
		Si (eleccion==2) Entonces
			// Punto fijo
			Escribir 'ingrese el valor inicial'
			Leer X0
			gx <- 0.5+0.2*SEN(X0)
			dergx <- 0.2*COS(X0)
			Si (abs(dergx)>1) Entonces
				Escribir 'No hay convergencia'
			SiNo
				Ea <- abs((gx-X0)/gx)*100
			FinSi
			Mientras Ea>Tolerancia Hacer
				ite <- ite+1
				X0 <- gx
				gx <- 0.5+0.2*SEN(X0)
				Ea <- abs((gx-X0)/gx)*100
			FinMientras
			Escribir 'iteracion ',ite
			Escribir 'La raiz es ',gx
			Escribir 'El error aprox. es ',Ea
		FinSi
	FinSi
FinAlgoritmo
