Algoritmo ejercicio3
	Tolerancia <- 0.5*10^(-1)
	ite <- 1
	Escribir 'Ingrese su opcion'
	Escribir '1) Newton Raphson'
	Escribir '2) Secante'
	Escribir 'otro salir'
	Leer eleccion
	Si (eleccion==1) Entonces
		// Newton Raphson
		Escribir 'Ingrese el valor inicial'
		Leer X0
		fx <- COS(X0)+(COS(X0))^2-1.2
		f1x <- -SEN(X0)-SEN(2*X0)
		f2x <- -COS(XO)-2*COS(2*X0)
		Si (abs((fx*f2x)/f1x^2)>1) Entonces
			Escribir 'No hay convergencia'
		SiNo
			X1 <- X0-(fx/f1x)
			Ea <- abs((X1-X0)/X1)*100
		FinSi
		Mientras Ea>Tolerancia Hacer
			ite <- ite+1
			X0 <- X1
			fx <- COS(X0)+(COS(X0))^2-1.2
			f1x <- -SEN(X0)-SEN(2*X0)
			f2x <- -COS(XO)-2*COS(2*X0)
			X1 <- X0-(fx/f1x)
			Ea <- abs((X1-X0)/X1)*100
		FinMientras
		Escribir 'iteracion ',ite
		Escribir 'La raiz es ',X1
		Escribir 'El error aprox. es ',Ea
	SiNo
		Si (eleccion==2) Entonces
			// Secante
			Escribir 'ingrese X1'
			Leer X1
			Escribir 'ingrese X2'
			Leer X2
			fX1 <- COS(X1)+(COS(X1))^2-1.2
			fX2 <- COS(X2)+(COS(X2))^2-1.2
			Si (fX1*fX2>0) Entonces
				Escribir 'no existe raiz'
			SiNo
				X3 <- X2-((fX2*(X1-X2))/(fX1-fX2))
			FinSi
			Ea <- 1000
			Mientras Ea>Tolerancia Hacer
				ite <- ite+1
				X1 <- X2
				X2 <- X3
				fX1 <- COS(X1)+(COS(X1))^2-1.2
				fX2 <- COS(X2)+(COS(X2))^2-1.2
				X3 <- X2-((fX2*(X1-X2))/(fX1-fX2))
				Ea <- abs((X3-X2)/X3)*100
			FinMientras
			Escribir 'iteracion ',ite
			Escribir 'La raiz es ',X3
			Escribir 'El error aprox. es ',Ea
		FinSi
	FinSi
FinAlgoritmo
